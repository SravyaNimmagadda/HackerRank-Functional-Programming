f :: [Int] -> [Int]
f (x:lst)  = (head lst) : f (tail lst)


main = do
   inputdata <- getContents
   mapM_ (putStrLn. show). f. map read. lines $ inputdata
